data = load('combined_lr_results.txt')




%surfc(data)
plot3(data(:,1), data(:,2), data(:,3), '*', 'Markersize', 6)
xlabel('Training Set Size', 'Fontsize', 14)
ylabel('Regularization Parameter C', 'Fontsize', 14)
zlabel('Validation Set Accuracy', 'Fontsize', 14)
title('Validation Results for Concatenated Abstract/2gram Data', 'Fontsize', 18)
set(gca,'yscale','log');
grid on

ind = find(data(:,3) == max(data(:,3)))

sz = data(ind,1)
c = data(ind,2)
acc = data(ind,3)

text(sz,c,acc, horzcat('Best Validation Accuracy:',  num2str(acc)))