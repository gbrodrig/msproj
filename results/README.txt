EM_cotrain.txt:
    At each iteration, label one example per classes, but do so probablistically.  In other words, make C copies of the example to be labeled where C is the number of classes.  Each copy is labeled as its class, but it is also given an associated weight.


