package com.rodrigues.msproj;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

	public class Execute_Query {
		
	public static BufferedWriter  getWriter(int i){
		BufferedWriter writer = null;
        try {
            //create a temporary file
        	
            File logFile = new File("./abstracts/"+Integer.toString(i) + ".txt");

            // This will output the full path where the file will be written to...
            System.out.println(logFile.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(logFile));
            return writer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
	}

	public static void main(String[] args) throws IOException {
		
		
		
		BasicConfigurator.configure();
		
		
		try {

			SignPostTest signPostTest = new SignPostTest();

			signPostTest.configureHttp();

			BufferedReader br = new BufferedReader(new FileReader("/home/garrett/workspace/errored_exes.txt"));
			JsonParser jp = new JsonParser();
		    try {
		        String [] line = br.readLine().split(",");
		        String exe_no = line[0];
		        String name = line[1].replace(" ", "+");
		        int error_count = 0;
		        while (line != null) {
		        	try{
		        		JsonElement je = jp.parse(signPostTest.executeSearch(name));
		        		JsonArray abstracts = je.getAsJsonObject().getAsJsonObject("bossresponse").getAsJsonObject("web").getAsJsonArray("results");
		        		String abs;
			        	BufferedWriter bw = getWriter(Integer.parseInt(exe_no));
			        	for(JsonElement j: abstracts){
			            	abs = j.getAsJsonObject().get("abstract").toString();
			            	bw.write(abs);
			            }
			        	bw.close();
		        	}catch(Exception e){
		        		error_count++;
		        		System.out.println("Problem with:" + line);
		        	}finally{
		        		 line = br.readLine().split(",");
				         exe_no = line[0];
				         name = line[1].replace(" ", "+");
		        		}
		        }
		        System.out.println(error_count);
		    } finally {
		        
		    	br.close();
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
