import cPickle
import scipy
import scipy.sparse as ssp
import sklearn
import numpy as np
from sklearn import linear_model
from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn import svm
from sklearn import ensemble
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pylab as p1
import random
import pdb

train_set_size = .70

def train_logistic(data,targets, C_vals):
    logreg = []
    if type(C_vals) != type([]):
        C_vals = [C_vals] 
    for reg_param in C_vals:
        mod = sklearn.linear_model.LogisticRegression(C=reg_param, penalty='l2')
        mod.fit(data,targets)
		#mod = linear_model.SGDClassifier(loss='log', penalty = 'l2', verbose = 0, n_iter = 100, alpha=reg_param, l1_ratio= 0, learning_rate = "constant", eta0 = .001)
        logreg += [mod]
    return logreg

def train_sgd(data, targets, C_vals, input_weights=None, cw='auto'):
    loss_dict = { 'hinge': 'SVM', 'log':'LogReg', 'squared_hinge':'Different SVM'}
    retMods = []
    with open('../what_the.txt', 'w') as f:
        f.write( str(cw));
    if type(C_vals) != type([]):
        C_vals = [C_vals] 
    for c in C_vals:
        print cw, 'hello world'
        mod = linear_model.SGDClassifier(loss='log', penalty = 'l2', verbose = 0, alpha = c, n_iter = 150, warm_start = True, class_weight=cw)
        if input_weights != None:
          mod.fit(data,targets, sample_weight = input_weights)
        else:
          mod.fit(data,targets)
        retMods += [mod]
    return retMods

def train_rf(data, targets, N_estimators):
  rfs = []
  for n_e in N_estimators:
      mod = ensemble.RandomForestClassifier(n_estimators = n_e)
      mod.fit(data,targets)
      rfs += [mod]
  return rfs

def tune_rf(data, targets, N_ests, output_file_name= None):
          print "Training the Random Forests ..."
          X_train, X_test, y_train, y_test = train_test_split(data,\
                          targets, random_state=0, train_size= train_size)
          trained_models = train_rf(X_train.todense(), y_train, N_ests)
          print "Predicting for the Validation set..."
          if output_file_name != None:
              f = open('../results/'+output_file_name,'a')
          for mod_no in range(len(trained_models)):
                  print "Num Estimators, " + str(N_ests[mod_no]) 
                  [acc,lr_y_pred] = predict_and_compare(X_test, y_test, trained_models[mod_no], True)
                  if output_file_name != None:
                      f.write(str(N_ests[mod_no]) + ","+str(acc))
                      f.write("\n")
                  #lr_cm = confusion_matrix(y_test, lr_y_pred)
                  print "---------------------------------"	
          if output_file_name != None:
                f.close()


     
def create_conf_matrix(y_pred, y_actual, title="Confusion Matrix"):
	cm  = confusion_matrix(y_actual, y_pred)
        print cm
        p1.matshow(cm)
	p1.colorbar()
	p1.title(title)
	p1.ylabel('True Label')
	p1.xlabel('Predicted Label')
	p1.show()

def predict_prob(X_test,y_actual,trained_model, needsDense = False, output_file_name = None):
        #print "Outputing predicted probabilities using the model"
        if not needsDense:
          y_pred = trained_model.predict_proba(X_test)
        else:
          y_pred = trained_model.predict_proba(X_test.todense())
        if output_file_name != None:
          y = open('../results/'+output_file_name, 'a')
          y.write(cPickle.dumps(y_pred))
          y.write("\n")
          y.close()
        return y_pred


def predict_and_compare(X_test,y_actual,trained_model, needsDense = False):
    if not needsDense:
      y_pred = trained_model.predict(X_test)
    else:
      y_pred = trained_model.predict(X_test.todense())
    inds = X_test.getcol(0).todense()
    toWrite = ""
    for i in range(y_pred.shape[0]):
      toWrite += str(inds[i]) + "," + str(y_pred[i]) + ","+str(y_actual[i]) +"\n"
    g = open('myFile.csv', 'w')
    g.write(toWrite)
    g.close()
    comparison = np.zeros((len(y_pred), 2))
    comparison[:,0] = np.array(y_actual)
    comparison[:,1] = np.array(y_pred)
    correct =np.equal(comparison[:,1], comparison[:,0]).sum()
    print "Number of correct predictions:" + str(correct)
    total = len(comparison[:,1])
    print "Total Number of Samples: " + str(total)
    acc = correct/float(total)
    print "Accuracy:"+str(float(correct)/total)
    return [acc,y_pred]

def train_svm(k_type,data,targets, C_vals):
	svms = []
	if type(C_vals) == int:
		C_vals = [C_vals] 
        sam = np.array([random.random() for x in range(len(targets))])
        print 'weights', sam.shape
	for reg_param in C_vals:
		mod = svm.SVC(C=reg_param, kernel=k_type)
                mod.fit(data,targets, sample_weight = sam)
		svms += [mod]
	return svms

def tune_bagger(data,targets,output_file_name = None, base_estimator=None, n_estimators=10, max_samples=1.0, max_features=1.0, bootstrap=True, bootstrap_features=False):
	bags = 4
        print "TUNING A BAGGING MODEL"
        if output_file_name != None:
                g = open('../results/' +output_file_name, 'a')         
        for j in range(1,1+bags):
          n_est = 10**j
	  X_train, X_test, y_train, y_test = train_test_split(data,\
			targets, random_state=0, train_size= train_set_size)
          bagger = ensemble.BaggingClassifier(n_estimators = n_est)
          bagger.fit(X_train.toarray(),y_train)
          [acc,y_preds] = predict_and_compare(X_test, y_test, bagger, True)
          print acc
          if output_file_name != None:
                g.write(str(n_est) +","+str(acc)+"\n")
        if output_file_name != None:
              g.close()

def train_random_forest(data,targets):
	rf = sklearn.ensemble.RandomForestClassifier()
	rf.fit(data.toarray(),targets)
	return rf

def tune_sgd_model(data, targets, C_values, output_file_name=None, varyTrainSize=False):
    print "Training the Logistic Model with SGD..."
    X_train_pre, X_test, y_train_pre, y_test = train_test_split(data,\
			targets, random_state=0, train_size= train_set_size)
    X_train_pre, X_unused, y_train_pre, y_unused = train_test_split(X_train_pre,\
			y_train_pre, random_state=0, train_size= .005)
    tVals = [1]
    if varyTrainSize:
        tVals = [.05, .1, .2, .3, .4, .5, .7, 1]
    for t in tVals:
      if t < 1:
          X_train, X_unused, y_train, y_unused = train_test_split(X_train_pre, y_train_pre, random_state=1, train_size= t)
      else:
          X_train, y_train = X_train_pre, y_train_pre
      weights = np.zeros((X_train.shape[0],)) + 1
      trained_models = train_sgd(X_train, y_train, C_values, weights)
      print "Predicting for the Validation set..."
      if output_file_name != None:
              f = open('../results/' +output_file_name, 'a') 
      for mod_no in range(len(trained_models)):
              print "C value: " + str(C_values[mod_no])
              print "Train size ", X_train.shape
              print "Test size", X_test.shape
              [acc,lr_y_pred] = predict_and_compare(X_test, y_test, trained_models[mod_no])
              if output_file_name != None:
                  f.write(str(train_size)+","+str(C_values[mod_no]) + ","+str(acc))
                  f.write("\n")
              print "---------------------------------"	
      if output_file_name != None:
        f.close()


def tune_logistic_model(data, targets, C_values, output_file_name=None, varyTrainSize=False):
    print "Training the Logistic Models..."
    X_train_pre, X_test, y_train_pre, y_test = train_test_split(data,\
			targets, random_state=0, train_size= train_set_size)
    tVals = [1]
    if varyTrainSize:
        tVals = [.05, .1, .2, .3, .4, .5, .7, 1]
    for t in tVals:
      if t < 1:
          X_train, X_unused, y_train, y_unused = train_test_split(X_train_pre, y_train_pre, random_state=1, train_size= t)
      else:
          X_train, y_train = X_train_pre, y_train_pre
      trained_models = train_logistic(X_train, y_train, C_values)
      print "Predicting for the Validation set..."
      if output_file_name != None:
              f = open('../results/' +output_file_name, 'a') 
      for mod_no in range(len(trained_models)):
              print "C value: " + str(C_values[mod_no])
              print "Train size ", X_train.shape
              print "Test size", X_test.shape
              [acc,lr_y_pred] = predict_and_compare(X_test, y_test, trained_models[mod_no])
              if output_file_name != None:
                  f.write(str(train_size)+","+str(C_values[mod_no]) + ","+str(acc))
                  f.write("\n")
              print "---------------------------------"	
      if output_file_name != None:
        f.close()


def tune_svm_models(data,targets,C_values, output_file_name = None):
	k_type = 'linear'
	print "Training an svm with a " +k_type+" kernel"
	train_size = .75
	X_train, X_test, y_train, y_test = train_test_split(data,\
			targets, random_state=0, train_size= train_size)
	trained_svms = train_svm(k_type,X_train, y_train, C_values)
	print "Predicting for the Validation set..."
	if output_file_name != None:
                f = open('../results/' +output_file_name, 'a')
	for mod_no in range(len(trained_svms)):
		print "C value: " + str(C_values[mod_no])
		[acc,lr_y_pred] = predict_and_compare(X_test, y_test, trained_svms[mod_no])
                if output_file_name != None:
                      f.write(str(train_size)+","+str(C_values[mod_no]) + ","+str(acc))
                      f.write("\n")
		print "---------------------------------"	
        if output_file_name != None:
	      f.close()	




def create_calibration_chart(model,data,labels, resolution =.1):
	numBins = int(1.0/resolution);
	probs = model.predict_proba(data)
	print "probabilities", probs.shape
	predictions = model.predict(data)
	counts = [0] * numBins 
	for prob in probs:
		myBin = int(np.round_(prob, decimal=1)*10)
		if myBin == 10:
			myBin = 9
		counts[myBin] = counts[myBin] +1
	fig,ax = pl.subplots()
	ind = range(0,1,resolution)
	width = .35
	rects1 = ax.bar(ind, counts, width, color = 'r')
	ax.set_ylabel('Counts')
	ax.set_title('Counts by Model Predicted Probability')
	ax.set_xticks(ind+width)
	ax.set_xticklabelsa([str(i) for i in ind])
	ax.legend( (rects1[0]), ('System'))
	autolabel(rects1)
	plt.show()

	
def train_ensemble(data1, data2, targets):
    num_feats_1 = data1.shape[1]
    num_feats_2 = data2.shape[1]
    pdb.set_trace()
    X_train_pre, X_test, y_train_pre, y_test = train_test_split(ssp.hstack((data1, data2)),\
			targets, random_state=0, train_size= train_set_size)
    X_train_pre, X_unused, y_train_pre, y_unused = train_test_split(X_train_pre,\
			y_train_pre, random_state=0, train_size= .005)
    data1_labeled = X_train_pre[:, :num_feats_1]
    data2_labeled = X_train_pre[:, num_feats_1:]

    model1 = train_sgd(data1_labeled, y_train_pre, C_vals=[1e-4], input_weights=None)[0]
    model2 = train_sgd(data2_labeled, y_train_pre, C_vals=[1e-4], input_weights=None)[0]

    firstProbs = model1.predict_proba(X_test[:, :num_feats_1])
    secondProbs = model2.predict_proba(X_test[:, num_feats_1:])

    avg = firstProbs + secondProbs
    avg = avg/2
    
    predictions = avg.argmax(axis=1)
    numTestExamples = y_test.shape[0]
    score = 1.0*np.equal(predictions, y_test).sum() / numTestExamples
    pdb.set_trace()
    print "The number of training examples is ", data1_labeled.shape[0] , data2_labeled.shape[0]
    print "The number of test examples are", numTestExamples
    print "the score is ", score 

    

    
	
