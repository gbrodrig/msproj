import numpy as np
import train_on_abstracts as ta
import train_on_ngrams as tn
#import labels as la
from perform_learning import *
import scipy.sparse as ssp
import sys, cPickle, random
from sklearn.cross_validation import KFold
import scipy as sp


import pdb

nowLabeledAbs = []
nowLabeledGrs = []


def load_both_datasets(dataset):
    [a_data, a_labels] = ta.load_data(dataset)
    ngram_data,ngram_labels = tn.load_data(dataset=dataset, n=[2,3], which='all_info')
    return [ngram_data, a_data, ngram_labels]

def generate_new_indices(training_data, training_targets, test_data, num_classes, beenLabeled, weights=None):
    my_mod =  train_sgd(training_data, training_targets, C_vals = [1e-4], input_weights=weights)[0]
    allWeights = my_mod.predict_proba(test_data) 
    num_new_examples = test_data.shape[0] 

    maxWeights = allWeights.max(axis=1) #take the max across all the rows
    classPred = allWeights.argmax(axis=1) #find the class that would have been predicted
    #variables to be returned
    newIndices =  []
    weights = []
    newLabels = []
    threshold = .9
    for i in range(num_new_examples):
        if maxWeights[i] > threshold and i not in beenLabeled:
            beenLabeled += [i]
            newIndices += [i]
            weights += [maxWeights[i]]
            newLabels += [classPred[i]]
    return my_mod, newIndices, weights, newLabels



def get_counts(targets):
      newLabels = la.new_labels
      for key in newLabels:
        newLabels[key] = 0
      for label in targets:
        newLabels[label] +=1
      for label in newLabels:
        print key, newLabels[key]

      
def incorporate_new_examples(oldExamples, oldLabels, unlabeledExamples, goodIndices, goodWeights, goodLabels):
    pdb.set_trace()
    newExamples = unlabeledExamples[goodIndices,:]
    #make sure to use the scipy.sparse implementation of vstack!
    allData = ssp.vstack((oldExamples, newExamples))
    allLabels = np.hstack( (oldLabels, goodLabels))
    

    labeled_weights = np.zeros((oldExamples.shape[0],))+1
    allWeights = np.hstack([labeled_weights, np.array(goodWeights)])
    return allData, allLabels, allWeights


if __name__ == "__main__":
        print "Cotraining on Abstract AND Bi-Tri DATA"
        print "Loading the Sparse Data Matrix and Labels..."
        dataset = '10k'
        [grams_data, abs_data, targets ] = load_both_datasets(dataset)
        num_classes = 2
        targets = np.equal(targets,5)
        [r_a, c_a] = abs_data.shape
        r_g, c_g = grams_data.shape
        data = ssp.hstack([abs_data, grams_data])

        #split the data into a training and test set
        X_train, X_final, y_train, y_final = train_test_split(data,\
                      targets, random_state=0, train_size= .7)

        #split the training data into sets we will use as labled and unlabeled
        X_t1, X_t2, y_t1, y_t2 = train_test_split(X_train,\
                    y_train, random_state=0, train_size= .05)

        absExamples = X_t1[:,0:c_a]
        grExamples = X_t1[:, c_a:]

        unlabeledAbsExs = X_t2[:,0:c_a]
        unlabeledGrExs = X_t2[:, c_a:]
        
        #STEP 0: 
        # Generate weights and labels for the unlabeled set
        gr_model, gr_inds, gr_weights, gr_labels = generate_new_indices(grExamples, y_t1, unlabeledGrExs,  num_classes, nowLabeledAbs)
        #abs_inds, abs_weights, abs_labels = generate_new_indices(absExamples, y_t1, X_abs_val, num_classes) 
       
        """
        pdb.set_trace()
        print "just using the training examples" 
        grams_model = train_sgd(absExamples, y_t1, 1e-4 )[0] 
        print grams_model.score(X_final[:,:c_a], y_final)
        
        print "with additional examples" 
        grams_model = train_sgd(absExamples, absLabels, 1e-4, absWeights)[0] 
        print grams_model.score(X_final[:,:c_a], y_final)
        pdb.set_trace() 
        exit(0)
        """
        

        ##PRINT OUT SOME BASELINE FIGURES BEFORE COTRAINING##
        print "Number of Training Examples in Baseline", absExamples.shape[0]
        baseAbsModel = train_sgd(absExamples, y_t1, C_vals=[1e-4])[0]
        baseAbsScore = baseAbsModel.score(X_final[:,0:c_a], y_final)
        print "BASELINE ABSTRACT SCORE: ", baseAbsScore 

        baseGrModel = train_sgd(grExamples, y_t1, C_vals=[1e-4])[0]
        baseGrScore = baseGrModel.score(X_final[:,c_a:], y_final)
        print "BASELINE NGRAM SCORE: ", baseGrScore
    
        for i in range(20):
            if i%2 == 0:
                print "The Abstracts Model"
                absExamples, absLabels, absWeights  = incorporate_new_examples(absExamples, y_t1, unlabeledAbsExs, gr_inds, gr_weights, gr_labels) 
                abs_model, abs_inds, abs_weights, abs_labels = generate_new_indices(absExamples, absLabels, unlabeledAbsExs, num_classes, nowLabeledGrs,absWeights)
                print absExamples.shape, absExamples.shape[0] > 7000
                print i, abs_model.score(X_final[:,0:c_a], y_final)
            else:
                print "The N-gram Model"
                grExamples, grLabels, grWeights  = incorporate_new_examples(grExamples, y_t1, unlabeledGrExs, abs_inds, abs_weights, abs_labels) 
                gr_model, gr_inds, gr_weights, gr_labels = generate_new_indices(grExamples, grLabels, unlabeledGrExs, num_classes, nowLabeledAbs, grWeights)
                print grExamples.shape, grExamples.shape[0] > 7000
                print i, gr_model.score(X_final[:,c_a:], y_final)


