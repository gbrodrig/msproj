import numpy as np
import pdb
from sklearn.externals import joblib


#res = joblib.load('../results/trials/cotraining_class_adjusted.pkl')
res = joblib.load('../results/trials/cotraining_class_adjusted_1.pkl')

def makePlot(trial_datasets, num_per_iter = 5):
    
    numSets = len(trial_datasets)
    myPlot = []
    for td in trial_datasets:
        nIter = len(td)
        shortest = None
        allPerfs = []
        for val in td:
            if not shortest:
                shortest = len(val)
            else:
                if len(val) < shortest:
                    shortest = len(val)
        for val in td:
            allPerfs += [val[:shortest]]
            
        standard = np.array(allPerfs)
        avg = standard.sum(axis=0)/nIter
        numExs = np.arange(len(avg))*num_per_iter+35
        return avg
        #myPlot += [plt.plot(numExs, avg)]
    """ 
    plt.axis([35, 300, .42, .5])
    plt.setp(myPlot[0], 'color', 'b', 'linewidth', .5, 'marker', 6)
    plt.setp(myPlot[1], 'color', 'r', 'linewidth', .5, 'marker', 6)
    plt.grid(True)
    plt.xlabel('Rounds of Cotraining')
    plt.ylabel('Accuracy')
    plt.title('Average Cotraining Accuracy vs Rounds of Training')
    """


avg = makePlot([res])

for value in avg:
    print value, ",",
