import train_on_abstracts as ta
import train_on_ngrams as tn
from perform_learning import *
import scipy.sparse as ssp
import sys, cPickle, random
def load_both_datasets(dataset, split=False):
    all_data = []
    print "Combining the abstract data with all the info in 2/3 gram data for the " + dataset + " set..."
    [a_data, labels] = ta.load_data(dataset) #for 100k data bi_tri_data, labels  = tn.load_data(1)
    all_data += [a_data]
    #ngram_data,labels = tn.load_data(dataset=dataset,n=[3], which='names_only')
    ngram_data,labels = tn.load_data(dataset=dataset, n=[2,3], which='all_info')
    all_data += [ngram_data]
    combined = ssp.hstack(all_data)
    if split:
        return all_data + [labels]
    else:
        return [combined, labels]


if __name__ == "__main__":

    print "TRAINING ON COMBINED DATA"
    #[data, targets ] = load_both_datasets('10k')
    [a_data, n_data, targets ] = load_both_datasets('10k', split=True)
    print "Finished loading data"
    #C_values = [10**i for i in range(-1,2)]
    #targets = np.equal(targets,5)
    out_file = None
    if len(sys.argv) > 1:
      out_file = sys.argv[1]
    #print "done training"
    import time
    s = time.time()
    C_values = [1.0]
    #tune_logistic_model(data,targets,C_values, varyTrainSize=True)
    C_values = [1e-4]
    train_ensemble(a_data, n_data, targets)
    #tune_sgd_model(data,targets,C_values)
    #tune_svm_models(data,targets,C_values, out_file)
    #tune_rf(data,targets, N_ests = [100, 1000])
    print time.time()-s, ' has elapsed'
        
   # tune_bagger(data,targets)

