
# coding: utf-8


import numpy as np
import train_on_abstracts as ta
import train_on_ngrams as tn
from perform_learning import *
import scipy.sparse as ssp
import sys, cPickle, random
import scipy as sp
import pdb

def load_both_datasets(dataset):
    [a_data, a_labels] = ta.load_data(dataset)
    ngram_data,ngram_labels = tn.load_data(dataset=dataset, n=[2,3], which='all_info')
    return [ngram_data, a_data, ngram_labels]


class Cotraining:
    
    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005):
        
        data = ssp.hstack([dataset_1, dataset_2])
        r_a, c_a = dataset_1.shape
        r_g, c_g = dataset_2.shape
        
        #split the data into a training and test set
        X_train, X_final, y_train, y_final = train_test_split(data, targets, random_state=0, train_size= t_size)
        
        #split the training data into sets we will use as labled and unlabeled
        Labeled, Unlabeled, labels, notUsed_labels = train_test_split(X_train, y_train, random_state=0, train_size= labeled_percent)

        ##Make the labeled data and noisy data attributes
        self.data1 = Labeled[:,0:c_a]
        self.data2 = Labeled[:, c_a:]
        
        self.trueLabels = labels
        
        self.unlabeled_1 = Unlabeled[:,0:c_a]
        self.unlabeled_2 = Unlabeled[:, c_a:]
        
        self.num_unlabeled = Unlabeled.shape[0]

        self.addedLabels_1 = []
        self.addedLabels_2 = []
        
        self.noisyLabels_1 = []
        self.noisyLabels_2 = []
        
        #make all the weights for examples with true labels equal to 1
        self.weights_1 = [1]*self.data1.shape[0]
        self.weights_2 = [1]*self.data2.shape[0]
        
        self.num_classes = 2
        
        self.X_final = X_final
        self.y_final = y_final
        
    
    def train_new_model(self, whichData_train):
        
        if whichData_train == 1:
            trueData = self.data1
            
            if self.addedLabels_2 != []:
                noisyLabeledData = self.unlabeled_1[self.addedLabels_2,:]
            else:
                noisyLabeledData = self.unlabeled_1[0,:]
                
            #Get the indices labeled by the SECOND dataset
            unlabeledIndices = np.setdiff1d(range(self.num_unlabeled), self.addedLabels_2)

            print len(unlabeledIndices) + len(self.addedLabels_2), 'should remain constant throughout'
            print "newly labeled, all uncertain", len(unlabeledIndices), len(unlabeledIndices) + len(self.addedLabels_2)

            unlabeledData = self.unlabeled_1[unlabeledIndices,: ]

            dataLabels = np.hstack((self.trueLabels, self.noisyLabels_2))
            weights = self.weights_2
        
        elif whichData_train == 2:
            trueData = self.data2
            
            if self.addedLabels_1 != []:
                noisyLabeledData = self.unlabeled_2[self.addedLabels_1,:]
            else:
                noisyLabeledData = self.unlabeled_2[0,:]
            
            #Get the indices labeled by the FIRST dataset
            unlabeledIndices = np.setdiff1d(range(self.num_unlabeled), self.addedLabels_1)

            print len(unlabeledIndices) + len(self.addedLabels_1), 'should remain constant throughout'
            
            unlabeledData = self.unlabeled_2[unlabeledIndices,: ]
            dataLabels = np.hstack((self.trueLabels, self.noisyLabels_1))
            weights = self.weights_1
           
            #Just a quick sanity check
            if len(unlabeledIndices) + len(self.addedLabels_1) > self.num_unlabeled+1:
                print "there is an error with your indexing"
                print len(unlabeledIndices) + len(self.addedLabels_1)
            
        #make sure to use the scipy.sparse implementation of vstack!
        allData = ssp.vstack((trueData, noisyLabeledData), format="csr")

        for ind in unlabeledIndices:
            if ind in self.addedLabels_2 and whichData_train == 1:
                print "WHAT THE FUCK"
        
        if whichData_train == 1 and self.addedLabels_2 ==[]:
            allData = allData[:-1, :]
        elif whichData_train == 2 and self.addedLabels_1 == []:
            allData = allData[:-1, :]
        #print allData.shape, dataLabels.shape, len(weights)
        
        my_mod =  train_sgd(allData, dataLabels, C_vals = [1e-4], input_weights=weights)[0]
        
        #scale the coefficients
        scaling_factor = .8
        my_mod.coef_ = my_mod.coef_ *scaling_factor
        my_mod.intercept_ = my_mod.intercept_ *scaling_factor
        
        allWeights = my_mod.predict_proba(unlabeledData) 
        
        num_new_examples = unlabeledData.shape[0] 
        maxWeights = allWeights.max(axis=1) #take the max across all the rows
        classPred = allWeights.argmax(axis=1) #find the class that would have been predicted
        #variables to be returned
        newIndices =  []
        weights = []
        newLabels = []
        threshold = .95
        #print maxWeights.shape
        #print classPred.shape
        #print num_new_examples

        pdb.set_trace()           
       
        for i,trueIndex in enumerate(unlabeledIndices):
            if whichData_train == 1 and trueIndex in self.addedLabels_1:
                print "I should have added this to group 1 already"
            if whichData_train == 2 and trueIndex in self.addedLabels_2:
                print "I should have added this to group 2 already"
            if maxWeights[i] > threshold:
                newIndices += [trueIndex]
                weights += [maxWeights[i]]
                newLabels += [classPred[i]]
        
        if len(newIndices) != 0:
            print "labeling some new data", len(newIndices)
        
        pdb.set_trace()           

        if whichData_train == 1:
            self.addedLabels_1 += newIndices
            self.noisyLabels_1 += newLabels
            self.weights_1 += weights
        else:
            self.addedLabels_2 += newIndices
            self.noisyLabels_2 += newLabels
            self.weights_2 += weights
            
        if len(weights) != 0:
            print 1.0*sum(weights)/len(weights), "Average example weight"
            
            
        return my_mod

    
    def start_cotraining(self):
        print "The number of examples with true labels is {0}".format(self.data1.shape[0])
        numFeatures_data1 = self.data1.shape[1] #number of features in dataset1
        for i in range(20):
            if i%2 == 0:
                print "The Abstracts Model", i
                mod1 = self.train_new_model(1)
                print i, mod1.score(self.X_final[:,0:numFeatures_data1], self.y_final)
                print '-'*30
            else:
                print "The N-gram Model", i
                mod2 = self.train_new_model(2)
                print i, mod2.score(self.X_final[:,numFeatures_data1:], self.y_final)
                print '-'*30
                
        def __str__(self):
            return "Not yet implemented"
                



if __name__ == "__main__":
    print "Cotraining on Abstract AND Bi-Tri DATA"
    print "Loading the Sparse Data Matrix and Labels..."
    dataset = '10k'
    [grams_data, abs_data, targets ] = load_both_datasets(dataset)
    grams_data = grams_data.tocsr()
    abs_data = abs_data.tocsr()
    num_classes = 2
    targets = np.equal(targets,5)
    
    ##PRINT OUT SOME BASELINE FIGURES BEFORE COTRAINING##
    trainingSet, X_final, trainingLabels, y_final = train_test_split(ssp.hstack((abs_data,grams_data)), targets, \
                    random_state=0, train_size= .7)
    Labeled, Unlabeled, labels, notUsed_labels = train_test_split(trainingSet, trainingLabels,\
                     random_state=0, train_size= .005)
    c_a = abs_data.shape[1]
    absSample= Labeled[:, c_a:]
    gramsSample= Labeled[:, :c_a]

    print "Number of Training Examples in Baseline", absSample.shape[0]
    baseAbsModel = train_sgd(absSample, labels, C_vals=[1e-4])[0]
    baseAbsScore = baseAbsModel.score(X_final[:, c_a:], y_final)
    print "BASELINE ABSTRACT SCORE: ", baseAbsScore 

    baseGrModel = train_sgd(gramsSample, labels, C_vals=[1e-4])[0]
    baseGrScore = baseGrModel.score(X_final[:, :c_a], y_final)
    print "BASELINE NGRAM SCORE: ", baseGrScore



    C = Cotraining(abs_data, grams_data, targets)

    C.start_cotraining()



