import cPickle
from perform_learning import *
from sklearn.feature_extraction.text import TfidfTransformer
from scipy.sparse import lil_matrix

def load_data_10k():
	print "Loading the TenK Sparse Data Matrix and Labels..."
	with open('../data/tenK/10k_vector_reps.pkl', 'r') as f:
		data_mat = cPickle.loads(f.read())
	with open('../data/tenK/labels.pkl', 'r') as g:
		labels = cPickle.loads(g.read())
        transformer = TfidfTransformer(norm='l2')
#        data_mat = transformer.fit_transform(data_mat)
        data_mat = lil_matrix(np.log(data_mat.todense()+1))
	return [data_mat, labels]

def load_data_100k():
	print "Loading the 100k Sparse Data Matrix and Labels..."
	with open('../data/100k/100k_vector_reps.pkl', 'r') as f:
		data_mat = cPickle.loads(f.read())
	with open('../data/100k/labels.pkl', 'r') as g:
		labels = cPickle.loads(g.read())
	return [data_mat, labels]


def load_data(choice='10k'):
    if choice=='100k':
        return load_data_100k()
    elif choice =='10k':
        return load_data_10k()
    else:
        print 'Invalid choice for loading data\n Input either "10k" or "100k"'
        exit(0)


def give_me_top_n(n,coeffs, output_file_name = None):
        vocab = cPickle.loads(open('../data/tenK/vocabulary_10k.txt', 'r').read())
        labels = {'': 10, 'Office': 4, 'Media': 2, 'Anti-Virus': 5, 'IT': 9, \
            'Util': 3, 'Game': 7, 'System/Other': 0, 'Internet': 1, 'Communication': 6, 'Network Apps': 8}
        labs = {}
        for key,val in labels.items():  
          labs[val] = key
        print vocab[:10]
        r,c = coeffs.shape
        print r,c
        maxE = 0
        finalList = []
        for row in range(r):
            myList = []
            c_mod_row = coeffs[row,:]
            z = sorted(c_mod_row)
            for item in z[-n:]:
                ind=list(c_mod_row).index(item)
                myList += [(vocab[ind], item)]
            finalList += [myList]
        if output_file_name != None:
            f = open(output_file_name, 'w')
            count = 0
            for doc in finalList:
              f.write(str(labs[count]))
              for w in doc:
                f.write(str(w))
                f.write("\n")
              f.write("\n")
              count +=1
            f.close()
        return finalList


if __name__ == "__main__":
    print "TRAINING ON ABSTRACT DATA"
    [data,targets] = load_data('10k')
    #C_values = [10**i for i in range(-4,5)]
    C_values = [1e-4]
	#Note the following two functions do not return anything and
	#instead write to their respective txt files in the results directory
    t_size = .75
	#model = train_logistic(X_train,y_train, C_values)
    #give_me_top_n(10,model[0].coef_, 'top_ten_by_indices.txt')
    tune_sgd_model(data,targets,C_values) #, varyTrainSize=True)#, 'abstracts_lr_results2.txt')
	#tune_svm_models(data,targets,C_values, None) #'abstracts_svm_results2.txt')
    #tune_rf(data, targets, range(10,101,10), 'results_rf_abstracts.txt')
