import cPickle
from perform_learning import *
from sklearn.cross_validation import train_test_split
import scipy.sparse as ssp
def load_data(dataset ='10k', n=[2], which='names_only'):
    if n == 2 or n == 3:
        n = [n]
    print "Loading the {0} dataset with {1} grams and the {2}".format(dataset, str(n), which)
    
    if dataset == '10k':
        basedir = "../data/tenK/" 
    elif dataset == '100k':
        basedir = "../data/100k/"
    else:
        print "bad dataset entered, choose either 10k or 100k"
        exit(0)
    dataFiles = []
    for gramSize in n:
        if which == 'names_only':
            ngram_vecs = [basedir+ str(gramSize)+"grams_names_only.txt"]
        elif which == 'all_info':
            ngram_vecs = [basedir+str(gramSize)+"grams_all_info.txt"]
        elif which == 'both':
            ngram_vecs = [basedir+ str(gramSize)+"grams_names_only.txt", basedir+str(n)+"grams_all_info.txt"]
        for myFile in ngram_vecs:
            with open(myFile, 'r') as f:
                dataFiles += [cPickle.loads(f.read())]
    allData = ssp.hstack(dataFiles)
    labs = basedir + "labels.pkl"
    with open(labs, 'r') as g:
        my_labs = cPickle.loads(g.read())
    return allData, my_labs

if __name__ == "__main__":
    print "Loading the NGRAM data..."
    #C_values =[10**i for i in range(-3,3)]
    C_values =[1e-4]
    dSet = '10k'
    [data, targets ] = load_data(dSet, [2,3], 'all_info')
    tune_sgd_model(data,targets,C_values)
    print '-'*50
    #tune_svm_models(data,targets,C_values, 'ngrams_svm_results.txt')
