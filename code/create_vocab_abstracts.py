#CREATE A VOCABULARY

import numpy as np
import scipy.sparse as ssp
import traceback
import os, cPickle, string
x = int(raw_input("type 0 for the 10k data and 1 for 100k data"))
if x == 1:
  base_dir = r'../data/100k/abstracts'
else:
  base_dir = r'../data/tenK/abstracts'

#word_dict key: value -> word: number_of_occurences
word_dict = {}
all_files = os.listdir(base_dir)
num_docs = len(all_files)
def build_vocab(base_dir):
	trans_table = string.maketrans("","")
	for f in all_files:
		with open(base_dir+"/"+f, 'r') as my_file:
			words = my_file.read()
			words = words.replace("<b>", "")
			words = words.replace("</b>", "")
			words = words.translate(trans_table, string.punctuation)
			individ_words = words.split()
			for word in individ_words:
			    if word not in word_dict:
				word_dict[word] = 1
			    else:
				word_dict[word] += 1
		if int(f.strip(".txt"))%200 == 0:
			print f
		
def strip_rare_frequent():
	vocab = []
	for word in word_dict:
		#only add words that don't happen in every document 
		#or only in 10 or more documents
		if word_dict[word] < 10000 and word_dict[word] > 10:
			vocab += [word]
	with open('../data/tenK/vocabulary_10k_2.txt', 'w') as vocab_file:
		vocab_file.write(cPickle.dumps(vocab))
        print 'the len of the vocab is ', len(vocab)

def optimize_lookup(vocab_list):
	vocab_dict = {}
        count = 0
	for word in vocab_list:
                vocab_dict[word] = vocab_list.index(word)
                count +=1
                if count%1000 == 0:
                  print count
        print "returning"
        with open('../data/tenK/vocab_dict_2.txt', 'w') as f:
          f.write(cPickle.dumps(vocab_dict))
        return vocab_dict

def create_vectors():
        print "Loading the giant vocabulary"
	vocab = cPickle.loads(open('../data/tenK/vocabulary_10k_2.txt', 'r').read())
        print "done loading my vocab of len: " + str(len(vocab))
	#vocab_dict is just to make indexing faster O(1) to find the index instead of O(n)
        try:
          vocab_dict = cPickle.loads(open("../data/tenK/vocab_dict_2.txt", "r").read())
        except:
          vocab_dict = optimize_lookup(vocab)
	trans_table = string.maketrans("","")
	num_words = len(vocab)
	j = 1
        error_indices = []

        for f in range(1,100001):
                num = f; 
	        file_vec = {}
		with open(base_dir+"/"+str(f)+".txt", 'r') as my_file:
			words = my_file.read()
			words = words.replace("<b>", "")
			words = words.replace("</b>", "")
			words = words.translate(trans_table, string.punctuation)
			individ_words = words.split()
			for word in individ_words:
				if word in vocab:
                                    if vocab_dict[word] in file_vec:
					file_vec[vocab_dict[word]] +=1
                                    else:
                                        file_vec[vocab_dict[word]] = 1
                with open('../data/tenK/10k_vector_representations_2.txt', 'a') as g:
                    for word_ind in file_vec:
                        toWrite = str(num) + "," +str(word_ind) +","+str(file_vec[word_ind])
                        g.write(toWrite+"\n")
                if f%100 == 0:
                    print f
def encode_classes():
        f = open('../data/100k/newExeClass_100k.txt', 'r')
	lines = f.readlines()
	code = 0
	code_dict = {}
	count_dict = {}
        count = 0
#	g = open('../data/tenK/100k_labels.txt', 'w')
        toWrite = []
	for exe in range(len(lines)):
		current_line = lines[exe].split(",")
                try:
                  label = current_line[4]
                except:
                  print current_line
                  print len(current_line)
		if label not in code_dict:
			code_dict[label] = code
			code +=1
                if label == "Anti-Virus":
                  count += 1
		toWrite += [code_dict[label]]
                if label not in count_dict:
                  count_dict[label] = 1
                else:
                  count_dict[label] +=1
	print code_dict
        print count_dict
        print count
#	g.write(cPickle.dumps(toWrite))
#       g.close()
#	f.close()
			

if __name__ == "__main__":
	print "Reading all abstract files and building a vocabulary"
	print "\tThis may take awhile"
	#build_vocab(base_dir)
	print "Now getting rid of all rare or frequent words"
	#strip_rare_frequent()
	print "Creating a vector representations of all documents"
	#create_vectors()
	print "Encoding the labels of all the classes into numbers"
	encode_classes()
