#!/usr/bin/python
import numpy as np
import train_on_abstracts as ta
import train_on_ngrams as tn
from perform_learning import *
import scipy.sparse as ssp
import sys, cPickle, random
from sklearn.cross_validation import KFold
from sklearn.preprocessing import normalize
import scipy as sp
import time
import pdb

# <codecell>

CWEIGHTS = {0:0.0786,\
                  1: 0.0017,\
                  2: 0.0867,\
                  3: 0.0513,\
                  4: 0.1875,\
                  5: 0.3546,\
                  6: 0.0015,\
                  7: 0.2104,\
                  8: 0.0017,\
                  9: 0.026
                  }

def load_both_datasets(dataset):
    print "Loading the Sparse Data Matrix and Labels..."
    [a_data, a_labels] = ta.load_data(dataset)
    ngram_data,ngram_labels = tn.load_data(dataset=dataset, n=[2,3], which='all_info')
    return [ngram_data, a_data, ngram_labels]

# <codecell>


class Baseclass:
    """
    Abstract base class is subclassed by Cotrainer, Self_Trainer, EM, Co_EM.

    @param dataset_1 one part of the feature split (should be a scipy spare or numpy array)
    @param dataset_2 the second part of the feature split
    @t_size : training set size.  What percentage used for training
    @labeled_percent: of the t_size used in training, what percent will be labeled
    @scaling_factor: to scale the logistic regression predictions
    """
    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005, scaling_factor = .8):
        self.scaling_factor = scaling_factor
        
        #put the training examples next to each other
        data = ssp.hstack([dataset_1, dataset_2])
        r_a, c_a = dataset_1.shape
        r_g, c_g = dataset_2.shape
        
        #split the data into a training and test set
        X_train, X_final, y_train, y_final = train_test_split(data,\
                      targets, random_state=int(time.time()), train_size= t_size)
        
        #split the training data into sets we will use as labled and unlabeled
        labeled, unlabeled_examples, labels, unused_labels = train_test_split(X_train,\
                    y_train, random_state=int(time.time()-50), train_size= labeled_percent)
        
        ##Make the labeled data and noisy data attributes
        self.data = [labeled[:,0:c_a], labeled[:, c_a:]]
        self.trueLabels = labels
        self._unusedLabels = unused_labels
          
        #Weight the class inversely proportional to their base rates
        self.c_weights = {}
        for label in self.trueLabels:
            if label not in self.c_weights:
                self.c_weights[label] = 1.0/CWEIGHTS[label]
        #print self.trueLabels, self.c_weights
        self.num_classes = len(set(labels))
        self.unlabeled = [unlabeled_examples[:,0:c_a], unlabeled_examples[:, c_a:]]
        
        self.addedLabels = []
        self.weights = [1]*self.data[0].shape[0]
        self.noisyLabels = [ ]

        self.models = [None, None]
        
        self.X_final = X_final
        self.y_final = y_final
        
        self.debug = False
        
    
    def train_model(self, whichSet):
        pass
    

    def label_new_examples(self, whichSet=0):
        pass
    
    def get_num_added_labels(self, whichSet=None):
        if self.selfTraining:
            addedLabels = self.addedLabels
        else:
            addedLabels = self.addedLabels[whichSet]
        return len(addedLabels)
        
    def start_training(self):
        pass
                
    def _ensembled_score(self):
        pass
        
    def __str__(self):
        return "Not yet implemented"
                

class EM(Baseclass):
    
    """
    Pure EM: At each iteration, train the one and only model.  Probablistically label all of the unlabeled data
        and use it to train the model at the next iteration 
    Feature Split: No
    Incremental/Iterative: Pure EM uses Iterative but can make it incremental (by specificing @param num_per_iter)
    Probablistic: Yes
    """
    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005, scaling_factor = .8):
        Baseclass.__init__(self, dataset_1, dataset_2, \
                            labels, t_size=.7, labeled_percent=.005, scaling_factor = .8)
        
        self.c_weights = None
        #get rid of the feature split
        self.data = ssp.hstack(self.data, format='csr')
        self.unlabeled = ssp.hstack(self.unlabeled, format='csr')
        
        #Only one model for EM
        self.models = train_sgd(self.data, self.trueLabels, C_vals=[1e-4])[0]
        self.models.warm_start = True
        
    def train_model(self):
        
        if self.addedLabels != []:
            allData = ssp.vstack([self.data, self.unlabeled[self.addedLabels,:]])
        else:
            allData = self.data
        
        #print "The number of examples used in training {0}".format(allData.shape[0])
        allLabels = np.hstack( [self.trueLabels, np.array(self.noisyLabels) ] )
        if self.addedLabels != []:
            
            """OPTIONAL: Train a new model each time
            self.models = train_sgd(allData, allLabels, input_weights = self.weights, C_vals=[1e-4])[0]
            """
                
            self.models.fit(allData, allLabels, sample_weight = self.weights)
        
    def label_new_examples(self):
        predictedProbs = self.models.predict_proba(self.unlabeled)
        predictedClassInd = predictedProbs.argmax(axis=1)
        predictedClass = [self.models.classes_[i] for i in predictedClassInd]
        
        weights = []
        unlabeledInds = []
        unlabeledClasses = []
        selfTraining = 0
        
        for ex in range(predictedProbs.shape[0]):
            for classInd in range(predictedProbs.shape[1]):
                trueClass = self.models.classes_[classInd]
                weights += [predictedProbs[ex,classInd]]
                unlabeledInds += [ex]
                unlabeledClasses += [trueClass]
        
        self.addedLabels = unlabeledInds
        self.noisyLabels = unlabeledClasses
        self.weights = [1]*self.data.shape[0] + weights

        
    def test_performance(self):
        return self.models.score(self.X_final, self.y_final)
        
    def start_training(self):
        #10 is arbitrary for now
        test_perf = []
        for i in range(15):
            self.train_model()
            test_perf += [self.test_performance()]
            print i, test_perf[i]
            self.label_new_examples()
        return test_perf
            
        


class Cotrainer(Baseclass):
    """
    Pure cotraining: At each iteration, train one model for each view.  Label one new example for each class and treat it 
        as if it were a ground truth label
    Feature Split: Yes
    Incremental/Iterative: Incremental (only label 1 new example per iteration)
    Probablistic: No (no weighting for the class)  i
    """
    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005, scaling_factor = .8):
        Baseclass.__init__(self, dataset_1, dataset_2, \
                            labels, t_size=.7, labeled_percent=labeled_percent, scaling_factor = .8)
        
        self.models[0] =  train_sgd(self.data[0], self.trueLabels, C_vals=[1e-4], input_weights = None, class_weight = self.c_weights)[0]
        self.models[1] =  train_sgd(self.data[1], self.trueLabels, C_vals=[1e-4], input_weights = None, class_weight = self.c_weights)[0]
        self.models[0].warm_start = True
        self.models[1].warm_start = True
        
    def train_model(self):
        
        if self.addedLabels != []:
            allData = [ssp.vstack([self.data[ii], self.unlabeled[ii][self.addedLabels,:] ]) for ii in range(2)]
        else:
            allData = self.data
        #print "The number of examples used in training {0}".format(allData[0].shape[0])
        allLabels = np.hstack( [self.trueLabels, np.array(self.noisyLabels) ] ) 
        
        self.models[0] = train_sgd(allData[0], allLabels, C_vals=[1e-4], input_weights = None, class_weight = self.c_weights)[0]
        self.models[1]= train_sgd(allData[1], allLabels, C_vals=[1e-4], input_weights = None, class_weight = self.c_weights)[0]
        
        
        
    def label_new_examples(self, iteration, num_per_iter = 1, class_ratios = None):
        
        numUnlabeled = self.unlabeled[0].shape[0] #really this should be the same for both
        unlabeledInds = np.setdiff1d( range(numUnlabeled), set(self.addedLabels) )
        notLabled_data = [self.unlabeled[i][unlabeledInds, :] for i in range(2)]
        
        allWeights = [self.models[i].predict_proba(notLabled_data[i]) for i in range(2)]
        maxPreds = [allWeights[i].max(axis=1) for i in range(2)]
        classInds = [allWeights[i].argmax(axis=1) for i in range(2)]
        
        sortedByMax = [sorted(zip(unlabeledInds, maxPreds[i], classInds[i]), key=lambda x: x[1], reverse=True) \
            for i in range(2)]
        
        
        if not class_ratios:
            needsLabel = [list(self.models[0].classes_)*num_per_iter, list(self.models[1].classes_)*num_per_iter]
        #some ratio preserving code
        else:
            needsLabel = [[], []]
            for c in class_ratios:
                if iteration % class_ratios[c]==0:
                    needsLabel[0] += [c]*num_per_iter
                    needsLabel[1] += [c]*num_per_iter
                    
        #if iteration % 100 == 0:
            #print iteration, needsLabel
            
        num_models = len(self.models)
        
        for jj in range(num_models):
            sorted_vals = sortedByMax[jj]  
            for ind, maxWeight, classInd in sorted_vals:
                className = self.models[jj].classes_[classInd]
                if className in needsLabel[jj] and ind not in self.addedLabels:
                    needsLabel[jj].remove(className)
                    self.addedLabels += [ind]
                    self.noisyLabels += [className]
                    #for cotraining, no weights are used
                if needsLabel[jj] == []:
                    break


        
    def test_performance(self):
        numFeatures_data1 = self.data[0].shape[1] #number of features in dataset1
        probs0 = self.models[0].predict_proba(self.X_final[:,0:numFeatures_data1])
        probs1 = self.models[1].predict_proba(self.X_final[:,numFeatures_data1:])
    
        combined = probs0*probs1
        combined = normalize(combined, axis=1, norm= 'l1')
        finalPredictions = combined.argmax(axis=1)
        finalPredictions = [self.models[0].classes_[i] for i in finalPredictions]
        
        correct = np.equal(finalPredictions, self.y_final).sum()
        
        return correct * 1.0 / len(finalPredictions)
    
        
    def start_training(self, numPerIter=1):
        print "Starting cotraining...."
        i = 0
        iter_perf = []
        class_rats = {0: 4, 1: 208, 2: 4, 3: 6, 4: 1, 5: 1, 6: 236, 7: 1, 8: 208, 9: 13}
        while len(self.addedLabels) != self.unlabeled[0].shape[0]:
            try:
                self.train_model()
                iter_perf += [self.test_performance()]
                if i%70 == 0:
                    print i, iter_perf[i], 'Remaining Examples',  self.unlabeled[0].shape[0] - len(self.addedLabels)
                self.label_new_examples(i, num_per_iter=numPerIter) #, class_ratios = class_rats)
            except:
                import traceback
                traceback.print_exc()
                print i-1, iter_perf[i-1], 'Remaining Examples',  self.unlabeled[0].shape[0] - len(self.addedLabels)
                inp = raw_input("do you want to continue cotraining")
                if inp != 'y' and inp != "Y":
                    print 'stopping'
                    return iter_perf
            i +=1
        return iter_perf
        

def makePlot(trial_datasets, num_per_iter = 5): 
    
    numSets = len(trial_datasets)
    myPlot = []
    for td in trial_datasets:
        nIter = len(td)
        shortest = None
        allPerfs = []
        for val in td: 
            if not shortest:
                shortest = len(val)
            else:
                if len(val) < shortest:
                    shortest = len(val)
        for val in td: 
            allPerfs += [val[:shortest]]
     
        standard = np.array(allPerfs)
        avg = standard.sum(axis=0)/nIter
        return avg
        #numExs = np.arange(le
        #myPlot += [plt.plot(numExs, avg)]
        
    plt.axis([35, 300, .42, .5])
    plt.setp(myPlot[0], 'color', 'b', 'linewidth', .5, 'marker', 6)
    plt.setp(myPlot[1], 'color', 'r', 'linewidth', .5, 'marker', 6)
    plt.grid(True)
    plt.xlabel('Rounds of Cotraining')
    plt.ylabel('Accuracy')
    plt.title('Average Cotraining Accuracy vs Rounds of Training')
    #fig = matplotlib.pyplot.gcf()
    #fig.set_size_inches(15,10)
    #fig.savefig('../results/cotraining_no_class_weight.png',dpi=100)

# <codecell>

class Self_Trainer(Baseclass):
    
    """
    The self-trainer does not use a feature split but adds new examples incrementally,
    one new example per iteration per class
    Feature Split: No
    Probablistic: Optional
    Iterative/Incremental: Incremental
    """
    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005, scaling_factor = .8):
        Baseclass.__init__(self, dataset_1, dataset_2, \
                            labels, t_size=.7, labeled_percent=.005, scaling_factor = 1)
        
        #get rid of the feature split
        self.data = ssp.hstack(self.data, format='csr')
        self.unlabeled = ssp.hstack(self.unlabeled, format='csr')
        
        #Only one model for Self-Training
        self.models = train_sgd(self.data, self.trueLabels, C_vals=[1e-4], class_weight = self.c_weights)[0]
        self.models.warm_start = True
        self.weights = [1] * self.data.shape[0]
        
    def train_model(self):
        
        if self.addedLabels != []:
            allData = ssp.vstack([self.data, self.unlabeled[self.addedLabels,:]])
        else:
            allData = self.data
        
        #hstack is concenating one the array is only 1-d
        allLabels = np.hstack( [self.trueLabels, np.array(self.noisyLabels) ] )
        if self.addedLabels != []:
            """
            
            #self.models.fit(allData, allLabels, sample_weight = self.weights)
            """
            self.models = train_sgd(allData, allLabels, C_vals=[1e-4], \
                                    input_weights=self.weights, class_weight = self.c_weights)[0]
            #self.models.fit(allData, allLabels) #no weighting
    def label_new_examples(self, num_per_iter = 10):
        
        
        numUnlabeled = self.unlabeled.shape[0] #really this should be the same for both
        unlabeledInds = np.setdiff1d(range(numUnlabeled), set(self.addedLabels))
        notLabled_data = self.unlabeled[unlabeledInds, :]
        
        allWeights = self.models.predict_proba(notLabled_data)
        maxPreds = allWeights.max(axis=1)
        classInds = allWeights.argmax(axis=1)
        
        sortedByMax = sorted(zip(unlabeledInds, maxPreds, classInds), key=lambda x: x[1], reverse=True)
        

        unlabeledInds = []
        unlabeledClasses = []
        weights = []
        
        needsLabel = list(self.models.classes_) * num_per_iter
        
         
        for ind, maxWeight, classInd in sortedByMax:
            className = self.models.classes_[classInd]
            if className in needsLabel and ind not in self.addedLabels:
                needsLabel.remove(className)
                self.addedLabels += [ind]
                self.noisyLabels += [className]
                weights += [maxWeight]
            if needsLabel == []:
                break
        
        self.weights += weights
        
    def test_performance(self):
        return self.models.score(self.X_final, self.y_final)
        
    def start_training(self):
        print "Beginning Self Training..."
        i = 0
        iter_perf = []
        while len(self.addedLabels) != self.unlabeled[0].shape[0]:
            try:
                self.train_model()
                iter_perf += [self.test_performance()]
                
                if i%50 == 0:
                    print "Number of training examples ", self.data.shape[0] + len(self.addedLabels)
                    print i, iter_perf[i], 'Remaining Examples',  self.unlabeled.shape[0] - len(self.addedLabels)
                self.label_new_examples(num_per_iter = 10)
                i+=1
            except:
                import traceback
                traceback.print_exc()
                return iter_perf
            
        return iter_perf
    
            
        
   

class CO_EM(Baseclass):
    """
    The coem does uses a feature split.  Both models are trained on their respective labeled examples.  Then model A probablistically labels some
        \ (possibly all)  of the unlabeled examples and model B then updates itself using the new labels and the repeats
    one new example per iteration per class
    Feature Split: Yes
    Probablistic: Yes
    Iterative/Incremental: Optional
    """

    def __init__(self, dataset_1, dataset_2, labels, t_size=.7, labeled_percent=.005, scaling_factor = .8):
        
        Baseclass.__init__(self, dataset_1, dataset_2, \
                            labels, t_size=.7, labeled_percent=.005, scaling_factor = .8)
        
        
        self.models[0] =  train_sgd(self.data[0], self.trueLabels, C_vals=[1e-4], class_weight = self.c_weights)[0]
        self.models[1] =  train_sgd(self.data[1], self.trueLabels, C_vals=[1e-4], class_weight = self.c_weights)[0]
        self.models[0].warm_start = True
        self.models[1].warm_start = True
        self.weights = [1]*self.data[0].shape[0] 
        
    def train_model(self, whichModel=0):
        """
        Except the first time, add all unlabeled examples to the training set with probablistic labels
        """
        
        if self.addedLabels != []:
            allData = ssp.vstack([self.data[whichModel], self.unlabeled[whichModel][self.addedLabels,:]])
        else:
            allData = self.data[whichModel]
        #print "The number of examples used in training {0}".format(allData[0].shape[0])
        allLabels = np.hstack( [self.trueLabels, np.array(self.noisyLabels) ] ) 
        self.models[whichModel] = train_sgd(allData, allLabels, C_vals=[1e-4], \
                                            input_weights = self.weights, class_weight = self.c_weights)[0] 
        
        
        
    def label_new_examples(self, whichModel=0, num_per_iter = None):
        """
        Probablistically Label All Examples
        With whichModel
        """
        numUnlabeled = self.unlabeled[whichModel].shape[0]
        if num_per_iter:
            unlabeledInds = np.setdiff1d(range(numUnlabeled), set(self.addedLabels) )
        else:
            unlabeledInds = range(numUnlabeled)
        
        
        predictedProbs = self.models[whichModel].predict_proba(self.unlabeled[whichModel][unlabeledInds,:]) 
        maxPreds = predictedProbs.max(axis=1)
        classInds = predictedProbs.argmax(axis=1)
        
        num_classes = predictedProbs.shape[1]
        num_unlabeled = predictedProbs.shape[0]
        classList = self.models[whichModel].classes_
        #the columns will be stacked on top of each other
        
        
        
        if not num_per_iter:
            #Iteratively Label All Examples
            weights = np.reshape( predictedProbs, (num_unlabeled*num_classes))
        
            # addedInds = [0 0 0 0 0 1 1 1 1 1 2 2 2 2  ... etc]
            addedInds = [ii/num_classes for ii in range(num_unlabeled*num_classes)]
            #noisyLabels[ 0 0 up to num_unlabeld followed by 1 1 1 1 ]
            noisyLabels = [classList[jj%num_classes] for jj in \
                           range(num_unlabeled*num_classes)]
            self.addedLabels = addedInds
            self.noisyLabels = noisyLabels
            #overwrite weights each time
            self.weights = np.hstack( [[1] * self.data[whichModel].shape[0], weights])
        else:
            #Incrementally Label num_per_iter examples per iteration per class
            
            needsLabel = list(self.models[whichModel].classes_) * num_per_iter
            
            sortedByMax = sorted(zip(unlabeledInds, maxPreds, classInds), key=lambda x: x[1], reverse=True)
            weights = []
            for ind, maxWeight, classInd in sortedByMax:
                className = self.models[whichModel].classes_[classInd]
                if className in needsLabel and ind not in self.addedLabels:
                    needsLabel.remove(className)
                    self.addedLabels += [ind]
                    self.noisyLabels += [className]
                    weights += [maxWeight]
                if needsLabel == []:
                    break
                    
            self.weights += weights

        
    def test_performance(self):
        
        numFeatures_data1 = self.data[0].shape[1] #number of features in dataset1
        probs0 = self.models[0].predict_proba(self.X_final[:,0:numFeatures_data1])
        probs1 = self.models[1].predict_proba(self.X_final[:,numFeatures_data1:])
        
            
        combined = probs0*probs1
        combined = normalize(combined, axis=1, norm= 'l1')
        finalPredictions = combined.argmax(axis=1)
        finalPredictions = [self.models[0].classes_[i] for i in finalPredictions]
       
        correct = np.equal(finalPredictions, self.y_final).sum()
        
        return correct * 1.0 / len(finalPredictions)
    
        
    def start_training(self, howMany=None):
        i = 0
        iter_perf = []
        for i in range(10):
            try:
                iter_perf += [self.test_performance()]
                self.train_model(i%2)
                
                print "Iteration:", i, "\tNum Added Labels:", len(self.addedLabels), "\tEnsembled Performance:", iter_perf[i]
                self.label_new_examples(i%2, num_per_iter = howMany)
                i+=1
            except:
                return iter_perf
        return iter_perf
        


if __name__ == "__main__":
    dataset = '10k'
    [grams_data, abs_data, targets ] = load_both_datasets(dataset)
    grams_data = grams_data.tocsr()
    abs_data = abs_data.tocsr()
    num_classes = 2
    binaryTargets = np.equal(targets,5)
