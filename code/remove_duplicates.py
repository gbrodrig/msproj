import numpy, cPickle

from scipy.sparse import lil_matrix
__author__ = "Garrett"



def load_data(decide):
        if int(decide) == 1:
          print 'reading the ten k dataset'
          with open('../data/tenK/newExeClass_10k.txt', 'r') as f:
                  data = f.readlines()
        else:
          with open('../data/100k/newExeClass_100k.txt', 'r') as f:
                  data = f.readlines()
	exe_names = []
	for line in data:
		exe_data = line.split(',')
                otherinfo = exe_data[-3:]+exe_data[-2:]+exe_data[-1:]
                #info = exe_data[0]+ otherinfo #uncomment for all info
                info = exe_data[0]# otherinfo
		exe_names += [info]
	return exe_names

def remove_duplicates(data_file):
    name_dict = {}
    count = 0
    new_copy = []
    for info in data_file:
      info = info.split(",")
      if info[0] not in name_dict:
        new_copy += [info]
        name_dict[info[0]] = 1
      else:
        print 'found a duplicate'
        count +=1 
        print count
    print len(name_dict)
    return new_copy



if __name__ == "__main__":
	decide = int(raw_input("use the 10k (1) dataset or 100k (0) dataset?"))
        dataset = load_data(decide)
        no_duplicates = remove_duplicates(dataset)
        y_n = raw_input("do you want to write to files y/n?\n")
        if y_n[0] == 'y':
          fn = raw_input("what do you want the output vector to be called")
          if decide == 1:
            path = '../data/tenK/'
            f = open(path+fn, 'w')
          else:
            path = '../data/100k/'
            f = open(path+fn, 'w')
          print "writing to " + path+fn
          f.write(cPickle.dumps(no_duplicates))
          f.close()
          
	
