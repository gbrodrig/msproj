import numpy, cPickle, pdb


from scipy.sparse import lil_matrix
__author__ = "Garrett"


#takes in list of ngram names and the value for n
# returns the vocab: a list of the non rare or overly frequent grams occurying in all words
# returns index_dict, a dictionary that gives the index (column number) of each word in the vocabulary
def build_vocab(dataset,n):
	vocab_dict = {}
        count = 0
        total = 0
	for exe in dataset:
                exe = exe.replace(" ", "")
		for j in range(len(exe)-n):
			gram = exe[j:j+n]
			if gram not in vocab_dict:
				vocab_dict[gram] = 1
                        else:
				vocab_dict[gram] +=1
                                count +=1
                        total +=1
        index_dict = {}
        badCount = 0
        vocab = []
	for gram in vocab_dict:
		if vocab_dict[gram] > 2 and vocab_dict[gram] < .8*len(dataset):
			vocab += [gram]
			index_dict[gram] = len(vocab)-1
                else:
                        badCount +=1
        print 'the size of the vocabulary after removing rare/frequents', len(index_dict)
        f = open('../data/100k/' + str(n) + 'gram_vocabulary.txt', 'w')
        f.write(cPickle.dumps(vocab))
        f.close()
	return [vocab, index_dict]

def build_vector_representations(index_dict, dataset, n):
	all_vectors = lil_matrix((len(dataset), len(index_dict)))
	for exe_num in range(len(dataset)):
		exe = dataset[exe_num]
		for j in range(len(exe)-n):
			gram = exe[j:j+n]
			if gram in index_dict:
				try:
					all_vectors[exe_num, index_dict[gram]] +=1
				except:
                                        print "there was an exception"
					print index_dict[gram], len(index_dict)
	return all_vectors


def load_data(decide):
        if int(decide) == 1:
          print 'reading the ten k dataset'
          with open('../data/tenK/newExeClass_10k.txt', 'r') as f:
                  data = f.readlines()
        else:
          with open('../data/100k/newExeClass_100k.txt', 'r') as f:
                  data = f.readlines()
	exe_names = []
	for line in data:
		exe_data = line.split(',')
                otherInfo = exe_data[-3]+exe_data[-2]+exe_data[-1]
                info = exe_data[0]+ otherInfo #uncomment for all info
                #info = exe_data[0]
		exe_names += [info]
        pdb.set_trace()
	return exe_names

def test_vector_rep(ind):
	print vectors.shape
	print dataset[ind]
	print vectors[ind,:]
	for j in range(len(v)):
		if vectors[ind,j] != 0:
			print v[j]
	


if __name__ == "__main__":
    decide = int(raw_input("use the 10k (1) dataset or 100k (0) dataset?"))
    dataset = load_data(decide)
    #n = int(raw_input('what value should i use for n?\n'))
    for n in range(2,4):
        [v, v_dict] = build_vocab(dataset,n)
        print 'the size of v_dict', len(v_dict), len(v)
        vectors = build_vector_representations(v_dict, dataset,n)
        #test_vector_rep(101))
        y_n = 'y' #raw_input("do you want to write to files y/n?\n")
        if y_n[0] == 'y':
            fn = str(n)+'grams_all_info.txt' #raw_input("what do you want the output vector to be called")
        if decide == 1:
            path = '../data/tenK/'
            f = open(path+fn, 'w')
        else:
            path = '../data/100k/'
            f = open(path+fn, 'w')
        print "writing to " + path+fn
        f.write(cPickle.dumps(vectors))
        f.close()
          
	
